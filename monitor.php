<?
	require_once("inc/loader.php");
	$page = new Page("template");
	$mysql = new DbMysql(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	$page->appendScript("javascripts/jquery.min.js");
	$page->appendScript("javascripts/jquery-ui-1.8.1.custom.min.js");
	$page->appendScript("javascripts/jquery.truncator.js");
	$page->appendScript("javascripts/jquery.dataTables.js");
	$page->appendStylesheet("stylesheets/demo_table.css");
	$page->appendStylesheet("stylesheets/demo_page.css");
	$page->appendStylesheet("stylesheets/cupertino/jquery-ui-1.8.1.custom.css");


	function getSubTree($mysql, $id){
		$q = $mysql->query("SELECT * FROM `hosts` WHERE `parent`='$id'");
		$ret = array();
		while($r = $q->fetch_array(MYSQLI_ASSOC)){
			$r['childs'] = getSubTree($mysql, $r['hostid']);
			$ret[] = $r;			
		}
		return $ret;
	}

	$hostid = intval($_GET['hostid']);
	$page->hostid = $hostid;
	$page->hosts = getSubTree($mysql, $hostid);
	$page->norefresh = false;
	if(isset($_GET['norefresh']))
		$page->norefresh = true;
	$page->load("monitor");

