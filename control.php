<?php
	include("inc/loader.php");

	if(isset($_GET['action'])){
		if( $_GET['action'] == "ping" && isset($_GET["ip"])){
			$ip = long2ip(ip2long($_GET['ip']));
			$ping = "ping " . CTL_PING_PARAMS . " $ip";
			$ping = `$ping`;
			print "<pre>$ping</pre>";
		}
		if( $_GET['action'] == "traceroute" && isset($_GET["ip"])){
			$ip = long2ip(ip2long($_GET['ip']));
			$traceroute = "traceroute " . CTL_TRACEROUTE_PARAMS . " $ip";
			$traceroute = `$traceroute`;
			print "<pre>$traceroute</pre>";
		}
		if( $_GET['action'] == "remove" && isset($_GET["hostid"]) && isset($_GET["ip"])){
			$page = new Page("template");
			$page->hostid = intval($_GET["hostid"]);
			$page->ip = long2ip(ip2long($_GET["ip"]));
			$page->load("remove");
		}
		if( $_GET['action'] == "removecat" && isset($_GET["hostid"]) ){
			$page = new Page("template");
			$page->hostid = intval($_GET["hostid"]);
			$page->load("removecat");
		}
		if( $_GET['action'] == "delete" && isset($_GET["hostid"]) ){
			$hostid = intval($_GET['hostid']);
			$mysql = new DbMysql(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			function getHostTree($mysql, $id){
				$q = $mysql->query("SELECT * FROM `hosts` WHERE `parent`='$id'");
				$ret = array();
				while($r = $q->fetch_array(MYSQLI_ASSOC)){
					$childs = getHostTree($mysql, $r['hostid']);
					$ret = array_merge($ret, $childs);
					$ret[] = $r['hostid'];
				}
				return $ret;
			}
			if($hostid != 0){
				$hosts = getHostTree($mysql, $hostid);
				$hosts = implode($hosts, ",");
				//print_r($hosts);
				$mysql->query("DELETE FROM `hosts` WHERE `hostid` in ({$hosts})");
				$mysql->query("DELETE FROM `hosts` WHERE `hostid` = '{$hostid}'");
			}
			header('location: monitor.php');
		}
		if( $_GET['action'] == "add" && isset($_GET['parent']) ){
			$page = new Page("template");
			$page->h_parent = intval($_GET['parent']);
			$page->load("add");
		}
		if( $_GET['action'] == "history" && isset($_GET["hostid"]) ){
			$page = new Page("template");
			$mysql = new DbMysql(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			$hostid = intval($_GET['hostid']);
			$q = $mysql->query("SELECT * FROM `history` WHERE `hostid` = '{$hostid}' ORDER BY date DESC");
			$page->history = array();
			while($r = $q->fetch_array(MYSQLI_ASSOC)){
				$page->history[] = $r;
			}
			$page->load("history");
		}
		
	} else {

		if( isset($_POST['action']) &&  $_POST['action']== "add" ){
			if( isset($_POST['parent']) && isset($_POST['descr']) ){
				$parent = intval($_POST['parent']);
				$ip = long2ip(ip2long($_POST['ip']));
				$descr = addslashes($_POST["descr"]);
				$cat = 0;
				if(isset($_POST['cat']))
					$cat = 1;
				$mysql = new DbMysql(DB_HOST, DB_USER, DB_PASS, DB_NAME);
				$mysql->query("INSERT INTO `hosts` (`ip`,`descr`,`is_category`,`parent`,`status`) VALUES ('{$ip}','{$descr}','{$cat}','{$parent}','DOWN')");
				header("location: monitor.php");
			} else {
				header("location: control.php?action=add&parent=".intval($_POST['parent']));
			}
		}

	}