<?
	require_once("inc/loader.php");
	$page = new Page("template");
	$mysql = new DbMysql(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	$page->appendScript("javascripts/jquery.min.js");
	$page->appendScript("javascripts/jquery.tree.js");
	$page->appendScript("javascripts/jquery.cookie.js");
	$page->appendScript("javascripts/jquery.tree.cookie.js");
	$page->appendScript("javascripts/jquery.tree.contextmenu.js");
	$page->appendStylesheet("stylesheets/tree.css");	
	$page->appendStylesheet("themes/apple/style.css");

	function getSubTree($mysql, $id){
		$q = $mysql->query("SELECT * FROM `hosts` WHERE `parent`='$id'");
		$ret = array();
		while($r = $q->fetch_array(MYSQLI_ASSOC)){
			if($r['is_category'] != 1){
				if($r['status'] == "UP")	$ret['up'] ++;
				if($r['status'] == "DOWN")	$ret['down'] ++;
			}
			$r['childs'] = getSubTree($mysql, $r['hostid']);
			$ret["up"] += $r["childs"]["up"];
			$ret["down"] += $r["childs"]["down"];
			$ret[] = $r;	
		}
		return $ret;
	}
	$page->hosts = getSubTree($mysql, -1);
	$page->load("tree");

