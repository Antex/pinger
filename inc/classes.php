<?php
	class Page {
		private $_template;
		private $_view;
		private $_viewFile;
		private $_styles;
		private $_scripts;

		public function __construct($template){
			$this->_template = $template;
			$this->_templateDir = dirname($_SERVER['SCRIPT_FILENAME']) . "/" . $this->_template;
		}

		public function appendStylesheet($filename){
			$this->_styles .= "<link href=\"{$filename}\" rel=\"stylesheet\"/>\n"; 
		}

		public function appendScript($filename){
			$this->_scripts .= "<script type=\"text/javascript\" src=\"{$filename}\"></script>\n"; 
		}

		public function load($view){
			if($this->_view != $view){
				$this->_view = $view;
				$this->_viewFile = $this->_templateDir . "/" . $this->_view . ".phtml";
				if(file_exists($this->_viewFile))
					require($this->_viewFile);
			}
		}
	}