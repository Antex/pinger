<?php

	class DbMysql extends Mysqli {
		public function __construct($db_host, $db_user, $db_pass, $db_name){
			parent::init();

			if (!parent::options(MYSQLI_OPT_CONNECT_TIMEOUT, 5)) {
				die('Setting MYSQLI_OPT_CONNECT_TIMEOUT failed');
			}

			if (!$this->real_connect($db_host, $db_user, $db_pass, $db_name)) {
				die('Connect Error (' . mysqli_connect_errno() . ') '
						. mysqli_connect_error());
			}

			$this->query("SET NAMES UTF8");
		}
	}